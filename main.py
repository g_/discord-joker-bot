import time
import jokerbot
import discord
from discord.ext.commands import Bot
import settings

joker = Bot(command_prefix="/")
last_joke = {}
saved_jokes = {}

@joker.event
async def on_ready():
	await joker.change_presence(game=discord.Game(name="with the Batman"))

@joker.event
async def on_command_error(args, kwargs):
	channel = kwargs.message.channel
	command = kwargs.invoked_with
	if (command == "getjoke"):
		global saved_jokes
		if (any(saved_jokes)):
			message = "Here are a list of saved jokes: \n `"
			for key, value in saved_jokes.items():
				message += "{}\n".format(key)
			message += " `\n"
			return await joker.send_message(channel, message)
		else:
			return await joker.send_message(channel, "No jokes have been saved, you can save jokes with /savejoke")

	if (command == "joke"):
		global last_joke

		joke = jokerbot.getJoke("")
		last_joke = joke
		await joker.send_message(channel, joke["joke"])
		time.sleep(settings.INTERNAL_JOKE_SLEEP_TIME)
		return await joker.send_message(channel, joke["punchline"])

	if (command == "savejoke"):
		return await joker.send_message(channel, "You need to specify what to call the joke")

@joker.command(help = "With no extra commands, gets a random joke. If extra commands are given, searches for a joke with any matching words")
async def joke(*, args):
	global last_joke
	await joker.say("Thinking...\n")
	joke = jokerbot.getJoke(args)
	#If no joke was returned, send a message telling that no joke was found
	if (joke == {}):
		return await joker.say("Couldn't find a joke...")
	last_joke = joke
	await joker.say(joke["joke"])
	time.sleep(settings.INTERNAL_JOKE_SLEEP_TIME)
	return await joker.say(joke["punchline"])

@joker.command(help = "Saves the joke with a given title. e.g. /savejoke joke")
async def savejoke(*args):
	global last_joke
	global saved_jokes
	if last_joke == {}:
		return await joker.say("No jokes were told before...")
	else:
		saved_jokes.update({args[0]:last_joke})
		return await joker.say("Joke has been saved, use /getjoke {} to tell it again".format(args[0]))

@joker.command(help = "Retieves the specified joke. e.g. /getjoke joke")
async def getjoke(args):
	global saved_jokes
	try:
		joke = saved_jokes[args]
		await joker.say(joke["joke"])
		time.sleep(settings.INTERNAL_JOKE_SLEEP_TIME)
		return await joker.say(joke["punchline"])
	except:
		return await joker.say("No joke found...")


if __name__ == "__main__":
	print("bot now running, use this link to invite the bot to your channel: \n")
	print("https://discordapp.com/api/oauth2/authorize?client_id={}&scope=bot&permissions=0".format(
		settings.DISCORD_CLIENT))
	joker.run(settings.DISCORD_BOT_TOKEN)