import praw
import settings

def getJoke(type):
	count = 0
	if type == "":
		parameters = ""
	else:
		parameters = type.split(" ")
	joke = {}

	reddit = praw.Reddit(user_agent=settings.REDDIT_USER_AGENT,
	                     client_id=settings.REDDIT_CLIENT_ID,
	                     client_secret=settings.REDDIT_CLIENT_SECRET,
	                     username=settings.REDDIT_USERNAME,
	                     password=settings.REDDIT_PASSWORD)

	for post in reddit.subreddit(settings.REDDIT_SUBREDDIT).hot():
		if count >= settings.INTERNAL_JOKE_TEST_COUNT:
			return joke
		sub = reddit.submission(id=post)
		title = sub.title
		text = sub.selftext
		if (parameters == ""):
			joke.update({"joke": title, "punchline": text})
			sub.hide()
			break
		else:
			for x in parameters:
				if (x in title.split(" ") or x in text.split(" ")):
					joke.update({"joke": title, "punchline": text})
					sub.hide()
					return joke

		count += 1
		#sub.hide()
	return joke